﻿using System;
using System.Net.Http;
using System.Text;
using Android.App;
using Android.Content;
using Newtonsoft.Json;

namespace Property24_Portal.Models
{
    public class service
    {
        HttpClient client = new HttpClient();
        Customer customer = null;
        public  async void GetCusts(string email, string password)
        {
            string url = @"http://10.0.2.2:8080/api/CustomersLogin?Email=" + email + "&Password=" + password + "";
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                customer = JsonConvert.DeserializeObject<Customer>(content);

                var loggedOn = Application.Context.GetSharedPreferences("CustomerList", FileCreationMode.Private);
                var loggedEdit = loggedOn.Edit();

                loggedEdit.PutInt("Customer_Id", JsonConvert.DeserializeObject<Customer>(content).Customer_Id);
                loggedEdit.PutString("Firstname", JsonConvert.DeserializeObject<Customer>(content).Firstname);
                loggedEdit.PutString("Lastname", JsonConvert.DeserializeObject<Customer>(content).Lastname);
                loggedEdit.PutString("Email", JsonConvert.DeserializeObject<Customer>(content).Email);
                loggedEdit.PutString("Password", JsonConvert.DeserializeObject<Customer>(content).Password);
                loggedEdit.PutString("Contact", JsonConvert.DeserializeObject<Customer>(content).Contact);
                loggedEdit.PutString("Gender", JsonConvert.DeserializeObject<Customer>(content).Gender);

                loggedEdit.Commit();
            }
        }

        public async void Update(Customer cust, int id)
        {
            string url2 = @"http://127.0.0.1:8080/api/UpdateCust?id=" + id;
            var json = JsonConvert.SerializeObject(cust);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = null;
            response = await client.PutAsync(url2, content);

            if(response.IsSuccessStatusCode)
            {
                GetCusts(cust.Email,cust.Password);
            }
        }
       
        public async void Register(Customer cust)
        {
            string url = "http://10.0.2.2:8080/api/Registers";
            var json = JsonConvert.SerializeObject(cust);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = null;
            response = await client.PostAsync(url, content);

            if (response.IsSuccessStatusCode)
            {
                GetCusts(cust.Email, cust.Password);
            }
        }

        public async void AddPic(Galleries gal)
        {
            string baseUrl = @"http://10.0.2.2:8080/api/api/UploadImage";
            var json = JsonConvert.SerializeObject(gal);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = null;
            response = await client.PostAsync(baseUrl, content);

            if (response.IsSuccessStatusCode)
            {
                
            }
        }

        public service()
        {
        }
    }
}
