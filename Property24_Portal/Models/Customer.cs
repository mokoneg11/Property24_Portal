﻿using System;
namespace Property24_Portal.Models
{
    public class Customer
    {
        private string text1;
        private string text2;
        private string text;

        public int Customer_Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Contact { get; set; }
        public string Gender { get; set; }


        public Customer(string firstname, string lastname, string email, string password)
        {
            Firstname = firstname;
            Lastname = lastname;
            Email = email;
            Password = password;
        }

        public Customer(int id, string firstname, string lastname, string email, string password, string number, string gen)
        {
            Customer_Id = id;
            Firstname = firstname;
            Lastname = lastname;
            Email = email;
            Password = password;
            Contact = number;
            Gender = gen;
        }
        public Customer(string firstname, string lastname, string email, string password, string number, string gen)
        {
            Firstname = firstname;
            Lastname = lastname;
            Email = email;
            Password = password;
            Contact = number;
            Gender = gen;
        }

        public Customer(string emails, string passwords)
        {
            Email = emails;
            Password = passwords;
        }
        public Customer()
        {
        }

        public Customer(string firstname, string lastname, string email, string password, string text) : this(firstname, lastname, email, password)
        {
            this.text = text;
        }
    }
}
