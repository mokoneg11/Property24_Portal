﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Newtonsoft.Json;

namespace Property24_Portal.Models
{
    public class Core
    {
        //Login
        public static async Task<Customer> GetCusts(string email, string password)
        {
            string url = @"http://10.0.2.2:8080/api/CustomersLogin?Email=" + email + "&Password=" + password + "";

            dynamic results = await DataService.getCustomerData(url).ConfigureAwait(false);

            if (results["Customers"] != null)
            {
                Customer cust = new Customer();
                cust.Customer_Id = (Int32)results["Customer_Id"];
                cust.Firstname = (string)results["Firstname"];
                cust.Lastname = (string)results["Lastname"];
                cust.Email = (string)results["Email"];
                cust.Password = (string)results["Password"];
                cust.Contact = (string)results["Contatct"];
                cust.Gender = (string)results["Gender"];
                return cust;
            }
            return null;
        }

        //Get customers to update their information
        //public static async Task<Customer> GetCust()
        //{
        //    string url = @"http://10.0.2.2:8080/api/Customer";

        //    dynamic results = await DataService.getCustomerData(url).ConfigureAwait(false);

        //    if (results["Customers"] != null)
        //    {
        //        var loggedInUser = Application.Context.GetSharedPreferences("Customers", FileCreationMode.Private);
        //        var loggedEdit = loggedInUser.Edit();

        //        loggedEdit.PutInt("Customer_Id", JsonConvert.DeserializeObject<Customer>(results).Customer_Id);
        //        loggedEdit.PutString("Firstname", JsonConvert.DeserializeObject<Customer>(results).Firstname);
        //        loggedEdit.PutString("Lastname", JsonConvert.DeserializeObject<Customer>(results).Lastname);
        //        loggedEdit.PutString("Email", JsonConvert.DeserializeObject<Customer>(results).Email);
        //        loggedEdit.PutString("Contact", JsonConvert.DeserializeObject<Customer>(results).Contact);
        //        loggedEdit.PutString("Gender", JsonConvert.DeserializeObject<Customer>(results).Gender);
        //        loggedEdit.Commit();
        //    }
        //    return null;
        //}


        //update Customers
        public static async Task<Customer> Update(Customer custs)
        {
            string url = @"http://10.0.2.2:8080/api/UpdateCust?Customer_Id=" + custs.Customer_Id + ";";

            dynamic results = await DataService.GetCustomer(url).ConfigureAwait(false);

            if (results["Customers"] != null)
            {
                //await GetCusts(custs.Email, custs.Password);
                Customer cust = new Customer();
                cust.Customer_Id = (Int32)results["Customer_Id"];
                cust.Firstname = (string)results["Firstname"];
                cust.Lastname = (string)results["Lastname"];
                cust.Email = (string)results["Email"];
                cust.Password = (string)results["Password"];
                cust.Contact = (string)results["Contatct"];
                cust.Gender = (string)results["Gender"];
                return cust;
            }
            return null;
        }

        public Core()
        {
        }
    }
}
