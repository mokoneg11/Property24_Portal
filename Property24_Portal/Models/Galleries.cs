﻿using System;
namespace Property24_Portal.Models
{
    public class Galleries
    {
        public int Imgae_Id { get; set; }
        public string Name { get; set; }
        public byte[] Images { get; set; }
        public int Prop_ID { get; set; }

        public Galleries(int img_id, string name,byte[] img, int prop)
        {
            Imgae_Id = img_id;
            Name = name;
            Images = img;
            Prop_ID = prop;
        }
        public Galleries(string name, byte[] imgPic, int Prop)
        {
            Name = name;
            Images = imgPic;
            Prop_ID = Prop;
        }

        public Galleries()
        {
        }
    }
}
