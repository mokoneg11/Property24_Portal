﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Newtonsoft.Json;

namespace Property24_Portal.Models
{
    public class DataService
    {
        public static async Task<dynamic> getCustomerData (string query)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(query);

            dynamic cust = null;
           
            if (response != null)

            {
                string json = response.Content.ReadAsStringAsync().Result;
                cust = JsonConvert.DeserializeObject(json); 

              
            }
            return cust;
        }

        public static async Task<dynamic> getCust(string query)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(query);

            dynamic cust = null;

            if (response != null)

            {
                string json = response.Content.ReadAsStringAsync().Result;
                cust = JsonConvert.DeserializeObject(json);
            }
            return cust;
        }

        public static async Task<dynamic> GetCustomer(string quer)
        {
            HttpClient client = new HttpClient();
            Customer customer = new Customer();

            //ISharedPreferences pref = Application.Context.GetSharedPreferences("CustomerList", FileCreationMode.Private);
            //var customers = pref.GetString("Customers", null);  

            var data = JsonConvert.SerializeObject(customer);
            var content = new StringContent(data, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(quer,content);

           dynamic cust = null;

            if (response != null)
            {
                
                string json = response.Content.ReadAsStringAsync().Result;
                cust = JsonConvert.DeserializeObject(json);
                //ISharedPreferencesEditor editor = pref.Edit();
                //editor.PutString("Customers", data);
                //editor.Commit();
            }
            return cust;
        }

        public DataService()
        {
        }
    }
}
