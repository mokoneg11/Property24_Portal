﻿using System;
namespace Property24_Portal.Models
{
    public class RESTAddresses
    {
        RESTAddresses adress = new RESTAddresses();

        public static String customerReg = @"http://10.0.2.2:8080/api/Registers";
        public static String customerLog = @"http://10.0.2.2:8080/api/CustomersLogin";
        public static String customerUpdate = @"http://10.0.2.2:8080/api/UpdateCust";
        public static String PropertyPost = @"http://10.0.2.2:8080/api/Property";
        public static String PropertyGet = @"http://10.0.2.2:8080/api/Property";
        public static String PropertyPut = @"http://10.0.2.2:8080/api/Property";
        public static String Email = @"http://10.0.2.2:8080/api/SendEmail";
        public static String ImageUpload = "";
        public static String ImageDownload = "";
    }
}
