﻿using System;
namespace Property24_Portal.Models
{
    public class EmailSend
    {
        public string senderName { get; set; }
        public string senderEmail { get; set; }
        public string senderContact { get; set; }
        public string Message { get; set; }

        public EmailSend(string sName, string sEmail, string sPhone, string msg)
        {
            senderName = sName;
            senderEmail = sEmail;
            senderContact = sPhone;
            Message = msg;
        }
        public EmailSend()
        {
        }
    }
}
