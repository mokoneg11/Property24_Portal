﻿using System;
namespace Property24_Portal.Models
{
    public class Gallery2
    {
        public int Gallery_Id { get; set; }
        public string File_Name { get; set; }
        public string FilePath { get; set; }

        public Gallery2(int id, string filen, string path)
        {
            Gallery_Id = id;
            File_Name = filen;
            FilePath = path;
        }
         
        public Gallery2( string filen, string path)
        {
            File_Name = filen;
            FilePath = path;
        }

        public Gallery2()
        {
        }
    }
}
