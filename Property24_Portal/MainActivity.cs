﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using Property24_Portal.Models;
using Android.Graphics;
using Android.Views;
using Android.Text;
using System;
using System.Collections;
using System.Linq;

namespace Property24_Portal
{
    [Activity(Label = "Property24", MainLauncher = true, Icon = "@drawable/P24", Theme = "@android:style/Theme.Holo")]
    public class MainActivity : Activity
    {
        static string uri = @"http://10.0.2.2:8080/api/Property";
        public static Context conxt;
        private static List<Property> prop = new List<Property>();
        static ListView listProp;

        private SearchView search;
        private static ArrayList PProperty;
        private ArrayAdapter<Property> adtp;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);


            listProp = FindViewById<ListView>(Resource.Id.listView1);
            GetProp propeo = new GetProp();
            propeo.Execute();

            search = FindViewById<SearchView>(Resource.Id.searchView1);
            search.SetQueryHint("Search Location");

            listProp.Adapter = new ProImageAdapter(this, prop);

            listProp.ItemClick += ListProp_ItemClick;
            search.QueryTextChange += Search_QueryTextChange;

            search.QueryTextSubmit += (sender, e) => {
                Toast.MakeText(this, "Searched for: " + e.Query, ToastLength.Short).Show();
                e.Handled = true;
            };
        }

        private void Search_QueryTextChange(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            //adtp.Filter.InvokeFilter(e.NewText);
            //listProp.TextFilter(e.NewText);
        }

        private void ListProp_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Toast.MakeText(this, adtp.GetItem(e.Position).ToString(), ToastLength.Long).Show();
        }

        public override bool OnCreateOptionsMenu(Android.Views.IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.PopMenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(Android.Views.IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.item1:
                    var intent = new Intent(this, typeof(LoginActivity));
                    StartActivity(intent);
                    return true;
                case Resource.Id.item2:
                    var intents = new Intent(this, typeof(RegisterActivity));
                    StartActivity(intents);
                    return true;
                case Resource.Id.item3:
                    var inten = new Intent(this, typeof(PropertyAddActivity));
                    StartActivity(inten);
                    return true;
                case Resource.Id.item4:
                    var inte = new Intent(this, typeof(ContactUsActivity));
                    StartActivity(inte);
                    return true;
                default:
                    return false;
            }
        }

        public class GetProp : AsyncTask
        {
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                HttpClient client = new HttpClient();

                Uri url = new Uri(uri);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync(url).Result;
                var property = response.Content.ReadAsStringAsync().Result;
                var result = JsonConvert.DeserializeObject<List<Property>>(property);

                foreach (var g in result)
                {
                    prop.Add(g);
                }
                return true;
            }
            protected override void OnPreExecute()
            {
                base.OnPreExecute();
            }
            protected override void OnPostExecute(Java.Lang.Object result)
            {
                base.OnPostExecute(result);
                listProp.Adapter = new ProImageAdapter(conxt, prop);
            }
        }

        public class ProImageAdapter : BaseAdapter<Property>
        {
            private List<Property> prope = new List<Property>();
            static Context context;
            public ProImageAdapter(Context con, List<Property> lstP)
            {
                prope.Clear();
                context = con;
                prope = lstP;
                this.NotifyDataSetChanged();
            }

            public override Property this[int position]
            {
                get
                {
                    return prope[position];
                }
            }

            public override int Count
            {
                get
                {
                    return prope.Count;
                }
            }
            public Context Mcontext
            {
                get;
                private set;
            }
            public override long GetItemId(int position)
            {
                return position;
            }

            public Bitmap getBitmap(byte[] getByte)
            {
                if (getByte.Length != 0)
                {
                    return BitmapFactory.DecodeByteArray(getByte, 0, getByte.Length);
                }
                else
                {
                    return null;
                }
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                View propertie = convertView;
                if (propertie == null)
                {
                    propertie = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.HomePage, parent, false);
                }
                TextView txtDesc = propertie.FindViewById<TextView>(Resource.Id.txtPropDesc);
                TextView txtType = propertie.FindViewById<TextView>(Resource.Id.txtPropType);
                TextView txtSta = propertie.FindViewById<TextView>(Resource.Id.txtPropSt);
                TextView txtAdres = propertie.FindViewById<TextView>(Resource.Id.txtAdress);
                TextView txtCt = propertie.FindViewById<TextView>(Resource.Id.txtCity);
                TextView txtprice = propertie.FindViewById<TextView>(Resource.Id.txtPrice);
                ImageView imView = propertie.FindViewById<ImageView>(Resource.Id.Image);

                if (prope[position].Images != null)
                {
                    imView.SetImageBitmap(BitmapFactory.DecodeByteArray(prope[position].Images, 0, prope[position].Images.Length));
                }

                txtDesc.Text = prope[position].Property_Desc;
                txtType.Text = prope[position].Property_Type;
                txtSta.Text = prope[position].Property_Stat;
                txtAdres.Text = prope[position].Address;
                txtCt.Text = prope[position].City;
                txtprice.Text = prope[position].Price;

                imView.Tag = prope[position].Images;
                return propertie;
            }
        }
    }
}