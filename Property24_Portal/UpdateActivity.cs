﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Property24_Portal.Models;

namespace Property24_Portal
{
    [Activity(Label = "UpdateActivity")]
    public class UpdateActivity : Activity
    {
       
        EditText txtId, txtFirst, txtLast, txtEmail, txtPass, txtPhone, txtGen;
        Button btnUpdate;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.UpdateUser);
            var loggedOn = Application.Context.GetSharedPreferences("CustomerList", FileCreationMode.Private);

            txtId = FindViewById<EditText>(Resource.Id.editId);
            txtFirst = FindViewById<EditText>(Resource.Id.editFirst);
            txtLast = FindViewById<EditText>(Resource.Id.editLast);
            txtEmail = FindViewById<EditText>(Resource.Id.editEmail);
            txtPass = FindViewById<EditText>(Resource.Id.editPass);
            txtPhone = FindViewById<EditText>(Resource.Id.editPhone);
            txtGen = FindViewById<EditText>(Resource.Id.editGen);


            btnUpdate = FindViewById<Button>(Resource.Id.btnEdit);
            btnUpdate.Click += BtnUpdate_ClickAsync;

            //txtFirst.SetText(loggedOn.GetInt("Customer_Id", 1), TextView.BufferType.Editable);
            txtFirst.SetText(loggedOn.GetString("Firstname", null), TextView.BufferType.Editable);
            txtLast.SetText(loggedOn.GetString("Lastname", null), TextView.BufferType.Editable);
            txtEmail.SetText(loggedOn.GetString("Email", null), TextView.BufferType.Editable);
            txtPass.SetText(loggedOn.GetString("Password", null), TextView.BufferType.Editable);
            txtPhone.SetText(loggedOn.GetString("Contact", null), TextView.BufferType.Editable);
            txtGen.SetText(loggedOn.GetString("Gender", null), TextView.BufferType.Editable);
        }

        private async void BtnUpdate_ClickAsync(object sender, EventArgs e)
        {
            try
            {
                service serve = new service();
                serve.Update(new Customer(txtFirst.Text, txtLast.Text, txtEmail.Text,txtPhone.Text, txtGen.Text),(Int32)txtId);

                Toast.MakeText(this, "User information updated successfully", ToastLength.Short).Show();
                Intent inter = new Intent(this, typeof(MainActivity));
                StartActivity(inter);
            }
            catch(Exception error)
            {
                Toast.MakeText(this, error.ToString(), ToastLength.Short).Show();
                Intent inters = new Intent(this, typeof(LoginActivity));
                StartActivity(inters);

            }

        }

    }
}
