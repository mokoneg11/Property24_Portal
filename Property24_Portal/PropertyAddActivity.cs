﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Property24_Portal.Models;

namespace Property24_Portal
{
    [Activity(Label = "PropertyAddActivity", MainLauncher = false)]
    public class PropertyAddActivity : Activity
    {
        HttpClient client;
        EditText txtDesc, txtType, txtStat, txtAdd, txtCity, txtBed, txtBath, txtGarage, txtPrice;
        static string uri = @"http://10.0.2.2:8080/api/Property";
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AddProperty);
            txtDesc = FindViewById<EditText>(Resource.Id.editDesc);
            txtType = FindViewById<EditText>(Resource.Id.editType);
            txtStat = FindViewById<EditText>(Resource.Id.editStat);
            txtAdd = FindViewById<EditText>(Resource.Id.editAddress);
            txtCity = FindViewById<EditText>(Resource.Id.editCit);
            txtBed = FindViewById<EditText>(Resource.Id.editBed);
            txtBath = FindViewById<EditText>(Resource.Id.editBath);
            txtGarage = FindViewById<EditText>(Resource.Id.editgar);
            txtPrice = FindViewById<EditText>(Resource.Id.editPrice);

            Button btn = FindViewById<Button>(Resource.Id.btnAddP);
            btn.Click += Btn_Clicked;

           Button btn2= FindViewById<Button>(Resource.Id.btnAddIm);
            btn2.Click += Btn2_Clicked;
        }

        private void Btn2_Clicked(object sender, EventArgs e)
        {
            Intent image = new Intent(this, typeof(GalleryActivity));
            StartActivity(image);
        }

        private async void Btn_Clicked(object sender, EventArgs e)
        {
            try
            {
                client = new HttpClient();
                var prop = new Models.Property
                {
                    Property_Desc = txtDesc.Text,
                    Property_Type = txtType.Text,
                    Property_Stat = txtStat.Text,
                    Address = txtAdd.Text,
                    City = txtCity.Text,
                    BedRoom = txtBed.Text,
                    BathRoom = txtBath.Text,
                    Garage = txtGarage.Text,
                    Price = txtPrice.Text
                };

                txtDesc.Text = "";
                txtType.Text = "";
                txtStat.Text = "";
                txtAdd.Text = "";
                txtCity.Text = "";
                txtBed.Text = "";
                txtBath.Text = "";
                txtGarage.Text = "";
                txtPrice.Text = "";

                Uri url = new Uri(String.Format(uri));
                var json = JsonConvert.SerializeObject(prop);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    Models.Property prope = JsonConvert.DeserializeObject<Models.Property>(data);

                    Toast.MakeText(this, "Successfully added property data", ToastLength.Short).Show();
                    Intent intent = new Intent(this, typeof(UpdateActivity));
                    StartActivity(intent);

                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.ToString(), ToastLength.Short).Show();
            }
        }
    }
}
