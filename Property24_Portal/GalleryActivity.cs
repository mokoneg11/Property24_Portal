﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Property24_Portal.Models;

namespace Property24_Portal
{
    [Activity(Label = "GalleryActivity", MainLauncher = false)]
    public class GalleryActivity : Activity
    {
        static string baseUrl = @"http://10.0.2.2:8080/api/api/UploadImage/";
        private ListView ilists = null;
        private BaseAdapter<Galleries> images = null;
        private List<Galleries> prop = null;
        private ImageView imgViews;
        private Galleries gal = null;
        public static Android.Content.Context context;
        public static readonly int PickImageId = 100;
        public ImageView imgSelected;
        Button btnAddImg;
        EditText txtImgName, txtPropId;
        public TaskCompletionSource<Stream> PickImageTaskCompletionSource { set; get; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.GalleryAdd);
            imgViews = FindViewById<ImageView>(Resource.Id.PropertyImg);
            btnAddImg = FindViewById<Button>(Resource.Id.SaveImg);
            txtImgName = FindViewById<EditText>(Resource.Id.ImageName);
            txtPropId = FindViewById<EditText>(Resource.Id.PropId);

           

            imgViews.Click += delegate {
                Intent intent = new Intent();
                intent.SetType("image/*");
                intent.SetAction(Intent.ActionGetContent);
                StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"), PickImageId);
            };
        }


        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            Galleries pimg = new Galleries();
            if (requestCode == PickImageId)
            {
                if ((resultCode == Result.Ok) && (data != null))
                {
                    try
                    {


                        Stream strem = ContentResolver.OpenInputStream(data.Data);
                        imgViews.SetImageBitmap(DecodeBitFromStream(data.Data, 150, 150));
                        Bitmap bitmap = BitmapFactory.DecodeStream(strem);
                        MemoryStream memS = new MemoryStream();
                        bitmap.Compress(Bitmap.CompressFormat.Webp, 100, memS); ;
                        byte[] pic = memS.ToArray();
                        string base64 = Convert.ToBase64String(pic);

                        btnAddImg.Click += async delegate
                        {
                            HttpClient client = new HttpClient();
                            Uri url = new Uri(baseUrl);

                            var myGallery = new Galleries
                            {
                                Name = txtImgName.Text,
                                Images = pic

                            };

                            var json = JsonConvert.SerializeObject(myGallery);
                            var content = new StringContent(json, Encoding.UTF8, "binary/json");

                            HttpResponseMessage response = null;

                            response = await client.PostAsync(url, content);

                            if (response.IsSuccessStatusCode)
                            {
                                var datas = await response.Content.ReadAsStringAsync();
                                Galleries gals = JsonConvert.DeserializeObject<Galleries>(datas);

                                Toast.MakeText(this, "Picture uploaded successfully", ToastLength.Long).Show();
                                Intent ip = new Intent(this, typeof(MainActivity));
                                StartActivity(ip);
                            }

                        //WebClient client = new WebClient();
                        //Uri uri = new Uri(baseUrl);
                        //client.Headers.Add("Content-Type", "application/json");
                        //pimg.Name = txtImgName.Text;
                        ////pic = imgViews.Tag;
                        //NameValueCollection param = new NameValueCollection();
                        ////param.Add("File_Name", Convert.ToString(gal.File_Name));
                        ////param.Add("FilePath", Convert.ToString(gal.FilePath));

                        //param.Add("Images", Convert.ToBase64String(pic));
                        //param.Add("Name", Convert.ToString(pimg.Name));
                        //string Result_msg = Encoding.UTF8.GetString(pic, 0, pic.Length);


                        //imgViews.SetImageResource(0);
                        //client.UploadValuesCompleted += Client_UploadValuesCompleted;

                        ////param.Add("Imgae_Id", imgId.ToString());
                        // //pimg.Images = client.UploadFile(baseUrl, "POST");
                        //// pimg.Name = client.UploadString(baseUrl, "POST");
                        ////pimg.Prop_ID = client.up(baseUrl, "Post");
                        //Toast.MakeText(this, "Successfully added Property Image/s", ToastLength.Long).Show();
                    };
                    }
                    catch(Exception ex)
                    {
                        Toast.MakeText(this, ex.ToString(), ToastLength.Long).Show();
                    }
                }
                else
                {
                    PickImageTaskCompletionSource.SetResult(null);
                }
            }
        }

      
        private Bitmap DecodeBitFromStream(Android.Net.Uri data, int requestWidth, int requestHeight)
        {
            Stream stream = ContentResolver.OpenInputStream(data);
            BitmapFactory.Options option = new BitmapFactory.Options();
            option.InJustDecodeBounds = true;
            BitmapFactory.DecodeStream(stream);

            option.InSampleSize = CalculateInSampleSize(option, requestWidth, requestHeight);

            stream = ContentResolver.OpenInputStream(data);
            option.InJustDecodeBounds = false;
            Bitmap bits = BitmapFactory.DecodeStream(stream, null, option);
            return bits;
        }

        private void Client_UploadValuesCompleted(object sender, UploadValuesCompletedEventArgs e)
        {
            RunOnUiThread(() =>
           {
                Console.WriteLine(Encoding.UTF8.GetString(e.Result));
            });
        }
    

        private int CalculateInSampleSize(BitmapFactory.Options option, int requestWidth, int requestHeight)
        {
            int  height = option.OutHeight;
            int  width = option.OutWidth;
            int inSampleSize = 1;

            if (height > requestHeight || width > requestWidth)
            {
                int halfHeight = height / 2;
                int halfWidth = width / 2;
                // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
                while ((halfHeight / inSampleSize) > requestHeight && (halfWidth / inSampleSize) > requestWidth)
                {
                    inSampleSize *= 2;
                }
            }
            return inSampleSize;
        }
    

    }
}