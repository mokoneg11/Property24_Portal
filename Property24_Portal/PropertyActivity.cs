﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Property24_Portal.Models;

namespace Property24_Portal
{
    [Activity(Label = "PropertyActivity", MainLauncher = true)]
    public class PropertyActivity : Activity
    {
        HttpClient client;
        EditText txtDesc, txtType, txtStat, txtAdress, txtCity, txtBed, txtBath, txtGarge, txtPrice;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AddProperty);

            txtDesc = FindViewById<EditText>(Resource.Id.editDesc);
            txtType = FindViewById<EditText>(Resource.Id.editType);
            txtStat = FindViewById<EditText>(Resource.Id.editStat);
            txtAdress = FindViewById<EditText>(Resource.Id.editAddress);
            txtCity = FindViewById<EditText>(Resource.Id.editCit);
            txtBed = FindViewById<EditText>(Resource.Id.editBed);
            txtBath = FindViewById<EditText>(Resource.Id.editBath);
            txtGarge = FindViewById<EditText>(Resource.Id.editgar);
            txtPrice = FindViewById<EditText>(Resource.Id.editPrice);

            Button button = FindViewById<Button>(Resource.Id.btnAddP);
            button.Click += Button_Clicked;

            Button btnImg = FindViewById<Button>(Resource.Id.btnAddIm);
            btnImg.Click += btnImg_Clicked;
        }

        private void btnImg_Clicked(object sender, EventArgs e)
        {
            Intent t1 = new Intent(this, typeof(MainActivity));
            StartActivity(t1); 
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
           
        }
    }
}
