﻿using System.Collections.Specialized;
using System.Net;
using System.Text;
using Android.App;
using Android.Views;
using Android.Content;
using Android.OS;
using Android.Widget;
using Org.Json;
using System;
using System.Threading.Tasks;
using Property24_Portal.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace Property24_Portal
{
    [Activity(Label = "LoginActivity")]
    public class LoginActivity : Activity 
    {
       
       
        TextView text;
        //GetCust gc;
        HttpClient client;
        EditText textE, textP;
        //EditText textF, textL, textC, textG;
        Button Login ;

        bool isLoggedIn = false;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Login);

            text = FindViewById<TextView>(Resource.Id.txtCreate);
            Login = FindViewById<Button>(Resource.Id.button1);

            Login.Click += Login_Click;
            text.Click += delegate {
                
                Intent ti = new Intent(this, typeof(RegisterActivity));
                StartActivity(ti);
            };
        }

        private  void Login_Click(object sender, EventArgs e)
        {
            try
            {
                service serve = new service();

                textE = FindViewById<EditText>(Resource.Id.editText1);
                textP = FindViewById<EditText>(Resource.Id.editText2);

                serve.GetCusts(textE.Text, textP.Text);

                if (!String.IsNullOrEmpty(textE.Text) && !String.IsNullOrEmpty(textP.Text))
                {
                    Toast.MakeText(this, "Successfully logged in", ToastLength.Short).Show();
                    Intent ti = new Intent(this, typeof(UpdateActivity));
                    StartActivity(ti);
                }
                    
            }catch(Exception ex)
            {
                Toast.MakeText(this, ex.ToString(), ToastLength.Short).Show();
            }

        }
    }
}
 