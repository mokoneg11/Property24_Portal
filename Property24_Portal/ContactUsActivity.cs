﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Property24_Portal
{
    [Activity(Label = "ContactUsActivity")]
    public class ContactUsActivity : Activity
    {
        EditText txtName, txtEmail, txtContact, txtMessage;
        Button btnSend;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ContactUs);

            txtEmail = FindViewById<EditText>(Resource.Id.editsenderE);
            txtContact = FindViewById<EditText>(Resource.Id.editNo);
            txtMessage = FindViewById<EditText>(Resource.Id.editEnquire);

            btnSend = FindViewById<Button>(Resource.Id.btnSend);

            btnSend.Click += (s, e) =>
             {
                 var email = new Intent(Intent.ActionSend);
                 email.PutExtra(Intent.ExtraEmail, new string[] { txtEmail.Text.ToString() });
                 email.PutExtra(Intent.ExtraSubject, txtContact.Text.ToString());
                 email.PutExtra(Intent.ExtraText, txtMessage.Text.ToString());

                 email.SetType("message/rfc822");
                 try
                 {
                     StartActivity(Intent.CreateChooser(email, "Send via"));
                 }
                 catch (ActivityNotFoundException ex)
                 {
                     Toast.MakeText(this, "There are no email applications installed." + ex.ToString(), ToastLength.Short).Show();
                 }
             };
        }


    }
}
